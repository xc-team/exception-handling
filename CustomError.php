<?php

namespace XcTeam\ExceptionHandling;

/**
 * 自定义错误类
 * @author mahaibo <mahaibo@hongbang.js.cn>
 */
class CustomError
{
    private static $rStatus;

    public static function register()
    {
        if (self::$rStatus) return;
        error_reporting(0);
        set_error_handler([new self, 'errorHandler']);
        set_exception_handler([new self, 'exceptionHandler']);
        register_shutdown_function([new self, 'fatalErrorHandler']);
    }

    public static function errorHandler($errno, $errstr, $errfile, $errline)
    {
        Output::errorOutput([
            'code' => 'SYSTEM_ERROR',
            'msg' => $errstr,
            'errorInfo' => [
                'file' => $errfile,
                'line' => $errline
            ]
        ]);
    }

    public static function exceptionHandler($exception)
    {
        $code = 'SYSTEM_ERROR';
        $msg = $exception->getMessage();
        $msgArr = explode('>>>', $msg);
        if (isset($msgArr[1])) {
            $msg = $msgArr[1];
            $code = $msgArr[0];
        }
        Output::errorOutput([
            'code' => $code,
            'msg' => $msg,
            'errorInfo' => [
                'file' => $exception->getFile(),
                'line' => $exception->getLine()
            ]
        ]);
    }

    public static function fatalErrorHandler()
    {
        $errorArr = error_get_last();
        if ($errorArr == null)
            exit();
        Output::errorOutput([
            'code' => 'SYSTEM_ERROR',
            'msg' => $errorArr['message'],
            'errorInfo' => [
                'file' => $errorArr['file'],
                'line' => $errorArr['line']
            ]
        ]);
    }
}

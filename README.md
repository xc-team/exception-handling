# 异常处理

使用

在入口使用函数'openExceptionHandling()'即可开启

主要功能
    自动捕获系统执行错误以及异常，并按照指定格式输出
    主动抛出异常，模块会接收处理

使用方法
    composer require xc-team/exception-handling

主动抛出异常示例
    throw new Exception('TEST_ERROR>>>这是一个测试项目');
    其中“TEST_ERROR”为错误码code，“这是一个测试项目”为错误提示

内置函数

```php

    /**
     * 开启异常处理
     * @return void
     */
    function openExceptionHandling(): void
    {
        CustomError::register();
    }

    /**
     * 设置错误输出格式
     * @param string $type 输出格式 默认json 目前支持json | xml | log (全小写)
     * @return void
     */
    function setErrorOutputFormat(string $type): void
    {
        Output::setOutputFormat($type);
    }
```
    
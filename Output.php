<?php

namespace XcTeam\ExceptionHandling;

/**
 * 错误输出类
 * @author mahaibo <mahaibo@hongbang.js.cn>
 */

class Output
{
    private static $outputFormat = 'json';

    public static function setOutputFormat($type)
    {
        self::$outputFormat = $type;
    }

    public static function errorOutput(array $errorArr)
    {
        header("HTTP/1.1 500");
        switch (self::$outputFormat) {
            case 'json':
                header("Content-type: application/json; charset=utf-8");
                $result = DataType::arrayToJson($errorArr, true);
                break;

            case 'xml':
                header("Content-type: application/xml; charset=utf-8");
                $result = DataType::arrayToXml($errorArr, 'xml');
                break;

            case 'html':
                header("Content-type: application/html; charset=utf-8");
                $result = DataType::arrayToHtml($errorArr);
                break;

            default:
                $result = DataType::arrayToLog($errorArr);
        }
        $saveDir = 'runtime' . DIRECTORY_SEPARATOR;
        if ($_SERVER['DOCUMENT_ROOT'] != '')
            $saveDir = $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . $saveDir;
        if (!file_exists($saveDir))
            mkdir($saveDir, 0777, true);
        error_log(DataType::arrayToLog($errorArr) . PHP_EOL, 3, $saveDir . date('Y-m-d'));
        exit($result);
    }
}

<?php

namespace XcTeam\ExceptionHandling;

use SimpleXMLElement;

/**
 * 数据转换类
 * @author mahaibo <mahaibo@hongbang.js.cn>
 */

class DataType
{
    public static function arrayToXml(array $array, $rootElement = 'xml', $xmlObj = null)
    {
        if ($xmlObj === null)
            $xmlObj = new SimpleXMLElement('<' . $rootElement . '/>');
        foreach ($array as $key => $value) {
            if (is_array($value))
                self::arrayToXml($value, $key, $xmlObj->addChild($key));
            else
                $xmlObj->addChild($key, $value);
        }
        return $xmlObj->asXML();
    }

    public static function arrayToJson(array $array, $isFormatting = false)
    {
        if ($isFormatting)
            return json_encode($array, JSON_UNESCAPED_UNICODE);
        return json_encode($array, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
    }

    public static function arrayToHtml(array $array)
    {
    }

    public static function arrayToLog(array $array, $mode = '1')
    {
        $head = '[' .  date('Y-m-d h-i-s') . '] ' . $array['code'] . ' | ' . $array['msg'];
        $location = $array['errorInfo']['file'] . ' line:' . $array['errorInfo']['line'];
        $requestStr = '';
        return $head . PHP_EOL . $location . PHP_EOL . $requestStr;
    }
}

<?php

use XcTeam\ExceptionHandling\CustomError;
use XcTeam\ExceptionHandling\Output;

if (!function_exists('openExceptionHandling')) {
    /**
     * 开启异常处理
     * @return void
     */
    function openExceptionHandling(): void
    {
        CustomError::register();
    }
}

if (!function_exists('setErrorOutputFormat')) {
    /**
     * 设置错误输出格式
     * @param string $type 输出格式 默认json 目前支持json | xml | log (全小写)
     * @return void
     */
    function setErrorOutputFormat(string $type): void
    {
        Output::setOutputFormat($type);
    }
}
